package baseDatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AgendaDbHelper  extends SQLiteOpenHelper {
    private  static final String TEXT_TYPE =" TEXT";
    private  static final String INTEGER_TYPE =" INTEGER";
    private  static final String COMMA_SEP = " , ";
    private  static final String SQL_CREATE_TABLE_CONTACTO  = " CREATE TABLE "
            + DefinirTabla.Contacto.TABLA_NAME +
             "( " + DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY," +
            DefinirTabla.Contacto.NOMBRE + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contacto.DIRECCION + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contacto.TELEFONO1 + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contacto.TELEFONO2 + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contacto.NOTAS + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contacto.FAVORITIO + INTEGER_TYPE +  ")";

    public static final String SQL_DELETE_CONTACT = "DROP TABLE IF EXISTS " + DefinirTabla.Contacto.TABLA_NAME;
    public static final int DATABASE_VERSIO  = 1;
    public  static final String DATABASE_NAME  = "agenda.db";







    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME,null, DATABASE_VERSIO);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_CONTACTO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_CONTACT);
        onCreate(sqLiteDatabase);

    }
}
