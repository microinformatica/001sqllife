package baseDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapplication.Contacto;

import java.lang.annotation.AnnotationFormatError;

public class AgendaContacto {

    Context context;
    AgendaDbHelper agendaDbHelper;
    SQLiteDatabase db;
    String []  columnsToRead = new String[]{
            DefinirTabla.Contacto._ID,
            DefinirTabla.Contacto.NOMBRE,
            DefinirTabla.Contacto.TELEFONO1,
            DefinirTabla.Contacto.TELEFONO2,
            DefinirTabla.Contacto.DIRECCION,
            DefinirTabla.Contacto.NOTAS,
            DefinirTabla.Contacto.FAVORITIO
    };

    public  AgendaContacto(Context context){
        this.context = context;
        this.agendaDbHelper = new AgendaDbHelper(this.context);

    }

    public  void  openDataBase(){
        db = agendaDbHelper.getWritableDatabase();


    }
    public long insertaContacto(Contacto c ){

        ContentValues values = new ContentValues();

        values.put(DefinirTabla.Contacto.NOMBRE,c.getNombre());
        values.put(DefinirTabla.Contacto.DIRECCION,c.getDomicilio());
        values.put(DefinirTabla.Contacto.TELEFONO1,c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2,c.getTelefono2());
        values.put(DefinirTabla.Contacto.NOTAS,c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITIO,c.isFavorito());

        return  db.insert(DefinirTabla.Contacto.TABLA_NAME,null,values);


    }
    public long updateContacto (Contacto c, long id){

        ContentValues values = new ContentValues();

        values.put(DefinirTabla.Contacto.NOMBRE,c.getNombre());
        values.put(DefinirTabla.Contacto.DIRECCION,c.getDomicilio());
        values.put(DefinirTabla.Contacto.TELEFONO1,c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2,c.getTelefono2());
        values.put(DefinirTabla.Contacto.NOTAS,c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITIO,c.isFavorito());

        String condicion = DefinirTabla.Contacto._ID + " = ?";
        String[] valoresCondicion = new String[]{String.valueOf(id)};

        return  db.update(DefinirTabla.Contacto.TABLA_NAME,values,condicion,valoresCondicion);


    }

    public  int deleteContacto (long id ){

        String condicion = DefinirTabla.Contacto._ID + " = ?";
        String[] valoresCondicion = new String[]{String.valueOf(id)};
        return db.delete(DefinirTabla.Contacto.TABLA_NAME,condicion,valoresCondicion);


    }

    public Contacto readContacto (Cursor cursor){

        Contacto c = new Contacto();
        c.setID(cursor.getLong(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorito(cursor.getInt(6));
        return  c;
    }


}
