package baseDatos;

import android.provider.BaseColumns;

public class DefinirTabla  {

    public  static  abstract  class Contacto implements BaseColumns{

        public static final  String TABLA_NAME = "contactosx";
        public static final  String NOMBRE = "nombre";
        public static final  String TELEFONO1 ="telefono";
        public static final  String TELEFONO2 = "telefono2";
        public static final String  DIRECCION = "direccion";
        public static final String NOTAS = "nota";
        public static final String FAVORITIO = "favorito";
    }

}
